#!/bin/bash

files=$@

for file in $files 
do
   if [ -f $file ]
      then
            echo "This file is regular file"
    elif [ -d $file ]
      then 
	    echo "This is a directory"
	    ls -all $file;

    elif [ -e $file ]
      then 
	    echo "This is a special file"
    else
            echo "File does not exist"
   fi
done
