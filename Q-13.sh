#!/bin/bash

echo "Please enter file name"

read file;

if [ -f $file ]
then 
	echo "This is regular file";
	exit 0
	
elif [ -d $file ]
then 
	echo "This is a directory";
	ls --all $file;
	exit 1
elif [ -e $file ]
then 
	echo "this is special file";
	exit 2
else 
	echo "File does not exist";
        exit 3
fi

