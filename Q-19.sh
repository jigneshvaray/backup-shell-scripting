#!/bin/bash

echo "Which directory's backup do you want ?"
echo "Please enter absolute path of directory"
read directory;
cd $directory

echo "enter name with .zip"
read name;
zip -r $name *

echo "please enter absolute destination path"

read destination;
work_dir=$(pwd);
if [ "${work_dir}" != "${destination}" ];
then
	cd $destination;
        if [ -e $name ];
	then 
	    echo "file already exist";
	    cd $directory
	    rm -rf $name;
	    exit 0;
	else
            cd $directory
	fi	    
else 
	echo "  "

fi 
mv $name $destination
rm -rf $name
