#!/bin/bash

echo "Please enter a file name or a directory path."

read FILE_PATH;

if [ -f $FILE_PATH ] 
   then 
	   echo "This file is regular file"
elif [ -d $FILE_PATH ]
  then 
	  echo "This is a directory"
	  ls --all $FILE_PATH;

elif [ -e $FILE_PATH ] 
  then
	echo "this is a special file"
else 
	echo "File does not exist"

fi 
