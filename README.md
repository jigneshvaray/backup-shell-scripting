
# Training

Tasks of shell scripts.

			                                           **Write script**

**Q-1**

Enter your name and age. In which year you will be 50 years old?.

**Q-2**

select color from option and output should be according to option for exapmle 1st color is blue if i enter value 1 then output should be blue color is good.

**Q-3**

write Shell script to compare two numbers.

**Q-4** 

write script to compare string.

**Q-5**

write script to remove file and print current time.

**Q-6**

write script to create a directory and in that directory create a 3 files.

**Q-7** 

Write a shell script to check to see if the file “file_path” exists.If it does exist, display “file_path passwords are enabled.” Next, check to see if you can write to the file. 
If you can, display “You have permissions to edit “file_path.””If you cannot, display “You do NOT have permissions to edit “file_path””

**Q-8** 

Create function in shell script.

**Q-9** 

create 4 shell script for all 4 type loops.

**Q-10**

Write a shell script that displays “man”,”bear”,”pig”,”dog”,”cat”,and “sheep” on the screen with each appearing on a separate line. Try to do this in as few lines as possible.

**Q-11**  

write a shell script that prompts the user for a name of a file or directory and reports if it is a regular file, a directory, or another type of file. Also perform an ls command against the file or directory with the long listing option.

**Q-12**   

Modify the previous script to that it accepts the file or directory name as an argument instead of prompting the user to enter it.
-Modify the previous script to accept an unlimited number of files and directories as arguments.

**Q-13** 

Write a shell script that accepts a file or directory name as an argument. Have the script report if it is reguler file, a directory, or another type of file. If it is a directory, exit with a 1 exit status. If it is some other type of file, exit with a 2 exit status.


**Q-14** 

Write a shell script that consists of a function that displays the number of files in the present working directory. Name this function “file_count” and call it in your script. If you use variable in your function, remember to make it a local variable.

**Q-15** 

Modify the script from the previous exercise. Make the “file_count” function accept a directory as an argument. Next, have the function display the name of the directory followed by a colon. Finally display the number of files to the screen on the next line. Call the function three times. First on the “/etc” directory, next on the “/var” directory and finally on the “/usr/bin” directory.


**Q-16** 

Write the shell script that renames all files in the current directory that end in “.jpg” to begin with today’s date in the following format: YYYY-MM-DD. For example, if a picture of my cat was in the current directory and today was October 31,2016 it would change name from “mycat.jpg” to “2016–10–31-mycat.jpg”.


**Q-17** 

create shell script. there are options CPU Information, CPU Utilization, memory usage, Disk Usage.

**Q-18** 

create 4 shell script for all 4 type loops.

**Q-19**

create a shell script to take backup of file. Prompt for source directory
then ask for name of zip if file already exist then show message that "file already exist"
and exit the script. If not exist then that file should move to destination directory and remove that zip file from source directory.







