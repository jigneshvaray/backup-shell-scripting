#!/bin/bash

echo "Please enter absolute File path or run this script from bin dir";

read FILE_PATH;

if [ -e "$FILE_PATH" ]
   then
         echo "Your expected file ' $FILE_PATH'  exists";

else
         echo "Your expected file '$FILE_PATH' does NOT exist";

fi

if [ -x "$FILE_PATH"  ]
   then 
	   echo "You have permissions to edit '$FILE_PATH' ";
else
	echo "You do NOT have permissions to edit '$FILE_PATH'";
fi
