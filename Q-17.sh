#!/bin/bash
#System Utilization 

echo To see details please enter number
echo 1. System Details
echo 2. Cpu Usage
echo 3. Memory Usage
echo 4. Disk Usage

while true; do
echo "Enter Your Choice";
read option

case $option  in 
	exit) break;;
	1) echo "CPU Information";
           lscpu;;
	2) echo "CPU Utilization";
	   mpstat -P all;;
	3) echo "Memory Usage";
	   cat /proc/meminfo;;
	4) echo "Disk Usage";
	   df -h;;
        *) echo "out of choice";
	   break;;

esac
done 
